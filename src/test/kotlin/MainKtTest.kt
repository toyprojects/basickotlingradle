import org.junit.jupiter.api.Assertions.*
import kotlin.test.Test

internal class MainKtTest {

    @Test
    fun testSquareRoot() {
        assertEquals(5.0, 25.0.squareRoot())
    }
}